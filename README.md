# IIS-Frontend

## Requirements
- node >= 8
- npm >= 6

## Installation
1. `npm i`

## Running
1. `npm run start`

## Links

- [react](https://reactjs.org/docs/getting-started.html)
- [prop-types](https://reactjs.org/docs/typechecking-with-proptypes.html)
- [create-react-app](https://facebook.github.io/create-react-app/docs/getting-started)
- [redux](https://redux.js.org/)
- [Immutable-js](https://facebook.github.io/immutable-js/docs/)
- [smacss](https://smacss.com/)
- [redux-thunk](https://medium.com/fullstack-academy/thunks-in-redux-the-basics-85e538a3fe60)
- [selectors](https://hackernoon.com/selector-pattern-painless-redux-store-destructuring-bfc26b72b9ae)
- [reselect](https://medium.com/@pearlmcphee/selectors-react-redux-reselect-9ab984688dd4)