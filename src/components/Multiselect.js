import React from 'react';
import PropTypes from 'prop-types';
import Select from "react-select";
import * as ImmutablePropTypes from "react-immutable-proptypes";


const Multiselect = ({placeholder, options, values, onChange, disabled}) => (
    <Select
        isMulti={true}
        placeholder={placeholder}
        options={options}
        disabled={disabled}
        value={options.filter((option) => values.some((value) => value === option.value)).toArray()}
        onChange={(options) => onChange && onChange(options.map((option) => option.value))}
    />
);

Multiselect.propTypes = {
    placeholder: PropTypes.string,
    disabled: PropTypes.bool.isRequired,
    values: PropTypes.oneOfType([PropTypes.array, ImmutablePropTypes.list]).isRequired,
    options: PropTypes.oneOfType([PropTypes.array, ImmutablePropTypes.list]).isRequired,
    onChange: PropTypes.func,
};

Multiselect.defaultProps = {
    disabled: false,
    values: []
};

export default Multiselect;