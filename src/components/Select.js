import React from 'react';
import PropTypes from 'prop-types';
import LibSelect from "react-select";
import * as ImmutablePropTypes from "react-immutable-proptypes";

const Select = ({options, value, onChange, disabled, placeholder}) => (
    <LibSelect
        classNamePrefix="select"
        placeholder={placeholder}
        options={options}
        disabled={disabled}
        value={options.find((option) => option.value === value)}
        onChange={(option) => onChange && onChange(option.value)}
    />
);

Select.propTypes = {
    disabled: PropTypes.bool.isRequired,
    placeholder: PropTypes.string,
    value: PropTypes.any,
    options: PropTypes.oneOfType([PropTypes.array, ImmutablePropTypes.list]).isRequired,
    onChange: PropTypes.func,
};

Select.defaultProps = {
    disabled: false,
};

export default Select;