import React from 'react';
import PropTypes from 'prop-types';


const Checkbox = ({checked, id, onChange, disabled, className}) => (
    <span className={`${className} ${disabled && 'checkbox-disabled'}`}>
        <input disabled={disabled} id={id} type="checkbox" checked={checked} onChange={(e) => onChange && onChange(e.target.checked)}/>
        <label htmlFor={id}/>
    </span>
);

Checkbox.propTypes = {
    disabled: PropTypes.bool.isRequired,
    checked: PropTypes.bool.isRequired,
    id: PropTypes.string.isRequired,
    className: PropTypes.string.isRequired,
    onChange: PropTypes.func,
};

Checkbox.defaultProps = {
    disabled: false,
    className: "checkbox"
};

export default Checkbox;