import React, {Component} from 'react';
import {toDateString} from "../../../utils/functions";
import Bug from "./Bug";
import PropTypes from "prop-types";

class Ticket extends Component {
    state = {open: false};

    resolvedBugCount = (ticket) => {
        return ticket.bugs.filter((bug) => bug.resolved).size;
    };

    bugCount = (ticket) => {
        return ticket.bugs.size;
    };

    render() {
        const {ticket} = this.props;
        return (
            <div className="ticket">
                <div className="ticket-header">
                    <span>Ticket #{ticket.id}</span>
                    <span>
                        <i className="icon icon-primary fas fa-user-circle"/> {ticket.created_by.username}
                    </span>
                    <span>
                        <i className="icon icon-primary fas fa-calendar-alt"/> {toDateString(ticket.created_at)}
                    </span>
                    <span>{Math.round((this.resolvedBugCount(ticket) / this.bugCount(ticket)) * 100)}%</span>
                </div>

                {this.state.open ? (
                    <React.Fragment>
                        <div>
                            <i className="action action-primary fas fa-chevron-up" onClick={() => this.setState({open: false})}/>
                        </div>
                        {ticket.bugs.map((bug) => <Bug bug={bug} />)}
                    </React.Fragment>
                ) : (
                    <React.Fragment>
                        <i className="action action-primary fas fa-chevron-down" onClick={() => this.setState({open: true})}/>
                    </React.Fragment>
                )}
            </div>
        );
    }
}

Ticket.propTypes = {ticket: PropTypes.instanceOf(Ticket).isRequired};

export default Ticket;