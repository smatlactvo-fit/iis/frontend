import ImmutablePropTypes from "react-immutable-proptypes";
import PropTypes from "prop-types";
import connect from "react-redux/es/connect/connect";
import {getAllModules} from "../../../modules/modules";
import Module from "../../../models/Module";
import {Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import React from "react";
import {List, Map} from "immutable";
import Select from "../../../components/Select";

const DEFAULT_STATE = {open: false, bugs: List()};
const PRIORITY_OPTIONS = [{value: 1, label: 'Nízká'}, {value: 2, label: 'Střední'}, {value: 3, label: 'Vysoká'}];

class TicketForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = DEFAULT_STATE;
        this.props.setOpenCallback(this.open);
    }

    open = ({title, onSave, onCancel}) => {
        this.setState({open: true, title, onSave, onCancel});
    };

    save = () => {
        this.state.onSave && this.state.onSave({bugs: this.state.bugs});
        this.setState(DEFAULT_STATE)
    };

    cancel = () => {
        this.state.onCancel && this.state.onCancel();
        this.setState(DEFAULT_STATE);
    };

    addBug = () => {
        this.setState((state) => ({...state, bugs: state.bugs.push(Map({priority: null, module: null, description: ''}))}));
    };

    editBug = (index, key, value) => {
        this.setState((state) => ({...state, bugs: state.bugs.setIn([index, key], value)}));
    };

    deleteBug = (index) => {
        this.setState((state) => ({...state, bugs: state.bugs.remove(index)}));
    };


    render() {
        const {bugs, open, title} = this.state;
        return (
            <Modal isOpen={open} size="lg">
                {title && <ModalHeader>{title}</ModalHeader>}
                <ModalBody>
                    {bugs.map((bug, index) => (
                        <div className="ticket-form">
                            <Select
                                placeholder={"Priorita"}
                                options={PRIORITY_OPTIONS}
                                value={bug.get('priority')}
                                onChange={(value) => this.editBug(index, 'priority', value)}
                            />
                            <Select
                                placeholder={"Modul"}
                                options={this.props.modules.map((module) => ({value: module.id, label: module.name}))}
                                value={bug.get('module')}
                                onChange={(value) => this.editBug(index, 'module', value)}
                            />
                            <textarea
                                placeholder={"Popis"}
                                value={bug.get('description')}
                                onChange={(e) => this.editBug(index, 'description', e.target.value)}
                            />
                            <i className="action action-primary fas fa-trash-alt" onClick={() => this.deleteBug(index)}/>
                        </div>
                    ))}

                    <i className="action action-primary fas fa-plus" onClick={this.addBug}/>
                </ModalBody>
                <ModalFooter className="modal-footer">
                    <button className="button button-primary" onClick={this.save}>Uložit</button>
                    <button className="button button-secondary" onClick={this.cancel}>Zrušit</button>
                </ModalFooter>
            </Modal>
        );
    }
}

TicketForm.propTypes = {
    modules: ImmutablePropTypes.listOf(PropTypes.instanceOf(Module)).isRequired,
    setOpenCallback: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({modules: getAllModules(state)});

export default connect(mapStateToProps)(TicketForm);