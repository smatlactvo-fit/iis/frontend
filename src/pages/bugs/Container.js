import React, {Component} from 'react';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import ImmutablePropTypes from "react-immutable-proptypes";
import PropTypes from 'prop-types';
import {createTicket, fetchAllTickets, getAllTickets} from "../../modules/tickets";
import Ticket from "./components/Ticket";
import {fetchAllModules} from "../../modules/modules";
import TicketForm from "./components/TicketForm";
import {openErrorModal} from "../../modules/modals";

class BugsContainer extends Component {
    componentDidMount() {
        this.props.fetchAllTickets();
        this.props.fetchAllModules();
    }

    create = () => {
        this.state.open({
            title: 'Vytvořit ticket',
            onSave: (data) => this.props.createTicket(data).catch(() => this.props.openErrorModal("Nepodařilo se vytvořit ticket"))
        })
    };

    render() {
        return (
            <div className="tickets">
                {this.props.tickets.map((ticket) => <Ticket ticket={ticket}/>)}
                <TicketForm setOpenCallback={(open) => this.setState({open})}/>
                <i className="action action-primary fas fa-plus" onClick={this.create}/>
            </div>
        );
    }
}

BugsContainer.propTypes = {
    tickets: ImmutablePropTypes.listOf(PropTypes.instanceOf(Ticket)).isRequired,
    fetchAllTickets: PropTypes.func.isRequired,
    fetchAllModules: PropTypes.func.isRequired,
    createTicket: PropTypes.func.isRequired,
    openErrorModal: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({tickets: getAllTickets(state)});
const mapDispatchToProps = (dispatch) => bindActionCreators({fetchAllTickets, fetchAllModules, createTicket, openErrorModal}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(BugsContainer);