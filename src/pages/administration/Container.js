import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import {Tab, TabList, TabPanel, Tabs} from "react-tabs";
import {bindActionCreators} from "redux";
import {fetchAllProgrammingLanguages} from "../../modules/programmingLanguages";
import ProgrammingLanguages from "./components/ProgrammingLanguages";
import Users from "./components/Users";
import {fetchAllUsers} from "../../modules/users";
import {fetchAllModules} from "../../modules/modules";
import Modules from "./components/Modules";
import {withRouter} from "react-router-dom";

const TABS = {0: 'uzivatele', 1: 'jazyky', 2: 'moduly'};

class AdministrationContainer extends Component {

    componentDidMount() {
        this.props.fetchAllProgrammingLanguages();
        this.props.fetchAllUsers();
        this.props.fetchAllModules();
    }

    render() {
        return (
            <Tabs
                selectedIndex={Number(Object.keys(TABS).find((key) => TABS[key] === this.props.match.params.tab) || 0)}
                onSelect={(index) => this.props.history.push(`/nastaveni/${TABS[index]}`)}
            >
                <TabList>
                    <Tab>UŽIVATELÉ</Tab>
                    <Tab>JAZYKY</Tab>
                    <Tab>MODULY</Tab>
                </TabList>

                <TabPanel>
                    <Users />
                </TabPanel>
                <TabPanel>
                    <ProgrammingLanguages />
                </TabPanel>
                <TabPanel>
                    <Modules />
                </TabPanel>
            </Tabs>
        );
    }
}

AdministrationContainer.propTypes = {
    fetchAllProgrammingLanguages: PropTypes.func.isRequired,
    fetchAllUsers: PropTypes.func.isRequired,
    fetchAllModules: PropTypes.func.isRequired,
};

const mapDispatchToProps = (dispatch) => bindActionCreators({fetchAllProgrammingLanguages, fetchAllUsers, fetchAllModules}, dispatch);

export default connect(null, mapDispatchToProps)(withRouter(AdministrationContainer));