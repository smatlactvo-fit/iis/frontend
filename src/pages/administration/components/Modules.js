import React, {Component} from 'react';
import ImmutablePropTypes from "react-immutable-proptypes";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {openErrorModal} from "../../../modules/modals";
import {createModule, deleteModule, getAllModules, updateModule} from "../../../modules/modules";
import Module from "../../../models/Module";
import ModuleForm from "./ModuleForm";

class Modules extends Component {
    edit = (module) => {
        this.state.open({
            module,
            title: 'Editovat modul',
            onSave: (data) => this.props.updateModule(module.id, data).catch(() => this.props.openErrorModal("Nepodařilo se upravit modul"))
        })
    };

    create = () => {
        this.state.open({
            title: 'Přidat modul',
            onSave: (data) => this.props.createModule(data).catch(() => this.props.openErrorModal("Nepodařilo se přidat modul"))
        })
    };

    delete = (module) => {
        this.props.deleteModule(module.id).catch(() => this.props.openErrorModal("Nepodařilo se smazat modul"));
    };

    render() {
        return (
            <React.Fragment>
                <table className="table">
                    <thead>
                        <tr>
                            <th>Název</th>
                            <th>Správce</th>
                            <th>Jazyk</th>
                            <th>Akce</th>
                        </tr>
                    </thead>
                    <tbody>
                    {this.props.modules.map((module, index) => (
                        <tr key={index}>
                            <td>{module.name}</td>
                            <td>{module.admin.username}</td>
                            <td>{module.lang.name}</td>
                            <td className="actions">
                                <i className="action action-primary fas fa-edit" onClick={() => this.edit(module)}/>
                                <i className="action action-primary fas fa-trash-alt" onClick={() => this.delete(module)}/>
                            </td>
                        </tr>
                    ))}
                    </tbody>
                </table>
                <ModuleForm setOpenCallback={(open) => this.setState({open})}/>
                <i style={{marginTop: "15px"}} className="action action-primary fas fa-plus" onClick={this.create}/>
            </React.Fragment>
        );
    }
}

Modules.propTypes = {
    modules: ImmutablePropTypes.listOf(PropTypes.instanceOf(Module)).isRequired,
    createModule: PropTypes.func.isRequired,
    updateModule: PropTypes.func.isRequired,
    deleteModule: PropTypes.func.isRequired,
    openErrorModal: PropTypes.func.isRequired,
};

const mapDispatchToProps = (dispatch) => bindActionCreators({createModule, updateModule, deleteModule, openErrorModal}, dispatch);
const mapStateToProps = (state) => ({modules: getAllModules(state)});

export default connect(mapStateToProps, mapDispatchToProps)(Modules);