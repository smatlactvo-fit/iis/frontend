import React from 'react';
import PropTypes from "prop-types";
import ImmutablePropTypes from "react-immutable-proptypes";
import ProgrammingLanguageForm from "./ProgrammingLanguageForm";
import {bindActionCreators} from "redux";
import {
    createProgrammingLanguage,
    updateProgrammingLanguage,
    deleteProgrammingLanguage,
    getLanguages
} from "../../../modules/programmingLanguages";
import {connect} from "react-redux";
import {openErrorModal} from "../../../modules/modals";
import ProgrammingLanguage from "../../../models/ProgrammingLanguage";

class ProgrammingLanguages extends React.Component {

    edit = (lang) => {
        this.state.open({
            lang,
            title: 'Editovat jazyk',
            onSave: (name) => this.props.updateProgrammingLanguage(lang.id, name).catch(() => this.props.openErrorModal("Nepodařilo se upravit jazyk"))
        })
    };

    create = () => {
        this.state.open({
            title: 'Přidat jazyk',
            onSave: (name) => this.props.createProgrammingLanguage(name).catch(() => this.props.openErrorModal("Nepodařilo se přidat jazyk"))
        })
    };

    delete = (lang) => {
        this.props.deleteProgrammingLanguage(lang.id).catch(() => this.props.openErrorModal("Nepodařilo se smazat jazyk"));
    };

    render() {
        return (
            <React.Fragment>
                <table className="table">
                    <thead>
                        <tr>
                            <th>Název</th>
                            <th>Akce</th>
                        </tr>
                    </thead>
                    <tbody>
                    {this.props.langs.map((lang, index) => (
                        <tr key={index}>
                            <td>{lang.name}</td>
                            <td className="actions">
                                <i className="action action-primary fas fa-edit" onClick={() => this.edit(lang)}/>
                                <i className="action action-primary fas fa-trash-alt" onClick={() => this.delete(lang)}/>
                            </td>
                        </tr>
                    ))}
                    </tbody>
                </table>
                <i style={{marginTop: "15px"}} className="action action-primary fas fa-plus" onClick={this.create}/>
                <ProgrammingLanguageForm setOpenCallback={(open) => this.setState({open})}/>
            </React.Fragment>
        );
    }
}

ProgrammingLanguages.propTypes = {
    langs: ImmutablePropTypes.listOf(PropTypes.instanceOf(ProgrammingLanguage)).isRequired,
    createProgrammingLanguage: PropTypes.func.isRequired,
    updateProgrammingLanguage: PropTypes.func.isRequired,
    deleteProgrammingLanguage: PropTypes.func.isRequired,
    openErrorModal: PropTypes.func.isRequired,
};

const mapDispatchToProps = (dispatch) => bindActionCreators({createProgrammingLanguage, updateProgrammingLanguage, deleteProgrammingLanguage, openErrorModal}, dispatch);
const mapStateToProps = (state) => ({langs: getLanguages(state)});

export default connect(mapStateToProps, mapDispatchToProps)(ProgrammingLanguages);