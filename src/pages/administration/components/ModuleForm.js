import React from 'react';
import PropTypes from 'prop-types';
import {Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import * as ImmutablePropTypes from "react-immutable-proptypes";
import ProgrammingLanguage from "../../../models/ProgrammingLanguage";
import {getLanguages} from "../../../modules/programmingLanguages";
import connect from "react-redux/es/connect/connect";
import {getAllUsers} from "../../../modules/users";
import User from "../../../models/User";
import Select from "../../../components/Select";
import {ROLES} from "../../../utils/constants";

const DEFAULT_STATE = {open: false, data: {name: '', admin: null, lang: null}, onSave: null, onCancel: null};

class ModuleForm extends React.Component {

    constructor(props) {
        super(props);

        this.state = DEFAULT_STATE;
        this.props.setOpenCallback(this.open);
    }

    open = ({module = {lang: {}, admin: {}}, title, onSave, onCancel}) => {

        const data = {name: module.name, admin: module.admin.id, lang: module.lang.id};
        this.setState({open: true, data: data, title, onSave, onCancel});
    };

    save = () => {
        this.state.onSave && this.state.onSave(this.state.data);
        this.setState(DEFAULT_STATE)
    };

    cancel = () => {
        this.state.onCancel && this.state.onCancel();
        this.setState(DEFAULT_STATE);
    };

    editFormData = (key, value) => {
        this.setState((state) => ({data: {...state.data, [key]: value}}));
    };

    render() {
        const {data, title, open} = this.state;
        return (
            <Modal isOpen={open}>
                {title && <ModalHeader>{title}</ModalHeader>}
                <ModalBody>
                    <label>Název</label>
                    <input type="text" value={data.name} onChange={(e) => this.editFormData('name', e.target.value)}/>
                    <label>Správce</label>
                    <Select
                        placeholder="Správce"
                        options={this.props.users.filter((user) => user.role === ROLES.ROLE_PROGRAMMER).map((user) => ({value: user.id, label: user.username}))}
                        value={data.admin}
                        onChange={(value) => this.editFormData('admin', value)}
                    />
                    <label>Jazyk</label>
                    <Select
                        placeholder="Jazyk"
                        options={this.props.langs.map((lang) => ({value: lang.id, label: lang.name}))}
                        value={data.lang}
                        onChange={(value) => this.editFormData('lang', value)}
                    />
                </ModalBody>
                <ModalFooter className="modal-footer">
                    <button className="button button-primary" onClick={this.save}>Uložit</button>
                    <button className="button button-secondary" onClick={this.cancel}>Zrušit</button>
                </ModalFooter>
            </Modal>
        );
    }
}

ModuleForm.propTypes = {
    langs: ImmutablePropTypes.listOf(PropTypes.instanceOf(ProgrammingLanguage)).isRequired,
    users: ImmutablePropTypes.listOf(PropTypes.instanceOf(User)).isRequired,
    setOpenCallback: PropTypes.func.isRequired,
};


const mapStateToProps = (state) => ({langs: getLanguages(state), users: getAllUsers(state)});

export default connect(mapStateToProps)(ModuleForm);