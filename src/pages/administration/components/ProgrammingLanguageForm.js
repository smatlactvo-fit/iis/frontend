import React from 'react';
import PropTypes from 'prop-types';
import {Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";

const DEFAULT_STATE = {open: false, name: '', onSave: null, onCancel: null};

class ProgrammingLanguageForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = DEFAULT_STATE;
        this.props.setOpenCallback(this.open);
    }

    open = ({lang = {}, title, onSave, onCancel}) => {
        this.setState({open: true, name: lang.name || '', title, onSave, onCancel});
    };

    save = () => {
        this.state.onSave && this.state.onSave(this.state.name);
        this.setState(DEFAULT_STATE)
    };

    cancel = () => {
        this.state.onCancel && this.state.onCancel();
        this.setState(DEFAULT_STATE);
    };

    render() {
        return (
            <Modal isOpen={this.state.open}>
                {this.state.title && <ModalHeader>{this.state.title}</ModalHeader>}
                <ModalBody>
                    <label>Název</label>
                    <input type="text" value={this.state.name} onChange={(e) => {this.setState({name: e.target.value})}}/>
                </ModalBody>
                <ModalFooter className="modal-footer">
                    <button className="button button-primary" onClick={this.save}>Uložit</button>
                    <button className="button button-secondary" onClick={this.cancel}>Zrušit</button>
                </ModalFooter>
            </Modal>
        );
    }
}

ProgrammingLanguageForm.propTypes = {
    setOpenCallback: PropTypes.func.isRequired,
};

export default ProgrammingLanguageForm;