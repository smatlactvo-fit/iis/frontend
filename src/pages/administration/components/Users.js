import React, {Component} from 'react';
import ImmutablePropTypes from "react-immutable-proptypes";
import PropTypes from "prop-types";
import User from "../../../models/User";
import {connect} from "react-redux";
import {createUser, getAllUsers, getLoggedUser, updateUser} from "../../../modules/users";
import {toDateString} from "../../../utils/functions";
import Checkbox from "../../../components/Checkbox";
import UserForm from "./UserForm";
import {bindActionCreators} from "redux";
import {openErrorModal} from "../../../modules/modals";
import {ROLE_LABELS} from "../../../utils/constants";

class Users extends Component {
    edit = (user) => {
        this.state.open({
            user,
            title: 'Editovat uživatele',
            onSave: (data) => this.props.updateUser(user.id, data).catch(() => this.props.openErrorModal("Nepodařilo se upravit uživatele"))
        })
    };

    create = () => {
        this.state.open({
            title: 'Přidat uživatele',
            onSave: (data) => this.props.createUser(data).catch(() => this.props.openErrorModal("Nepodařilo se přidat uživatele"))
        })
    };

    render() {
        return (
            <React.Fragment>
                <table className="table">
                    <thead>
                        <tr>
                            <th>Jméno</th>
                            <th>Datum narození</th>
                            <th>Role</th>
                            <th>Jazyky</th>
                            <th>Aktivní</th>
                            <th>Akce</th>
                        </tr>
                    </thead>
                    <tbody>
                    {this.props.users.map((user, index) => (
                        <tr key={index}>
                            <td>{user.username}</td>
                            <td>{toDateString(user.birthday)}</td>
                            <td>{ROLE_LABELS[user.role]}</td>
                            <td>{user.langs.map((lang) => lang.name).join(', ')}</td>
                            <td><Checkbox disabled={true} checked={user.active} id={`${index}-active-checkbox`} /></td>
                            <td className="actions">
                                {(user.id !== this.props.loggedUser.id) && (
                                    <i className="action action-primary fas fa-edit" onClick={() => this.edit(user)}/>
                                )}
                            </td>
                        </tr>
                    ))}
                    </tbody>
                </table>
                <UserForm setOpenCallback={(open) => this.setState({open})}/>
                <i style={{marginTop: "15px"}} className="action action-primary fas fa-plus" onClick={this.create}/>
            </React.Fragment>
        );
    }
}

Users.propTypes = {
    users: ImmutablePropTypes.listOf(PropTypes.instanceOf(User)).isRequired,
    loggedUser: PropTypes.instanceOf(User).isRequired,
    createUser: PropTypes.func.isRequired,
    updateUser: PropTypes.func.isRequired,
    openErrorModal: PropTypes.func.isRequired,
};

const mapDispatchToProps = (dispatch) => bindActionCreators({createUser, updateUser, openErrorModal}, dispatch);
const mapStateToProps = (state) => ({users: getAllUsers(state), loggedUser: getLoggedUser(state)});

export default connect(mapStateToProps, mapDispatchToProps)(Users);