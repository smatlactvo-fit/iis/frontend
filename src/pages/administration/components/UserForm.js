import React, {Component} from 'react';
import {Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import PropTypes from "prop-types";
import {ReactDatez} from "react-datez";
import {DATE_FORMAT, ROLES, ROLE_LABELS} from "../../../utils/constants";
import moment from "moment";
import {connect} from "react-redux";
import {getLanguages} from "../../../modules/programmingLanguages";
import ProgrammingLanguage from "../../../models/ProgrammingLanguage";
import * as ImmutablePropTypes from "react-immutable-proptypes";
import Checkbox from "../../../components/Checkbox";
import Multiselect from "../../../components/Multiselect";
import Select from "../../../components/Select";

const DEFAULT_STATE = {open: false, data: {username: '', birthday: '', active: true, langs: [], password: ''}, onSave: null, onCancel: null};
const ROLE_OPTIONS = [{value: ROLES.ROLE_ADMIN, label: ROLE_LABELS[ROLES.ROLE_ADMIN]}, {value: ROLES.ROLE_PROGRAMMER, label: ROLE_LABELS[ROLES.ROLE_PROGRAMMER]}, {value: ROLES.ROLE_USER, label: ROLE_LABELS[ROLES.ROLE_USER]}];

class UserForm extends Component {
    constructor(props) {
        super(props);

        this.state = DEFAULT_STATE;
        this.props.setOpenCallback(this.open);
    }

    open = ({user, title, onSave, onCancel}) => {
        const editing = !!user;
        user = user || DEFAULT_STATE.data;

        const data = {username: user.username, birthday: user.birthday, active: user.active, langs: user.langs.map((lang) => lang.id)};
        this.setState((state => ({open: true, editing, data: {...state.data, ...data}, title, onSave, onCancel})));
    };

    save = () => {
        this.state.onSave && this.state.onSave(this.state.data);
        this.setState(DEFAULT_STATE)
    };

    cancel = () => {
        this.state.onCancel && this.state.onCancel();
        this.setState(DEFAULT_STATE);
    };

    editFormData = (key, value) => {
        this.setState((state) => ({data: {...state.data, [key]: value}}));
    };

    render() {
        const {data, title, editing, open} = this.state;
        return (
            <Modal isOpen={open}>
                {title && <ModalHeader>{title}</ModalHeader>}
                <ModalBody>
                    <label>Jméno</label>
                    <input type="text" value={data.username} onChange={(e) => this.editFormData('username', e.target.value)}/>
                    <label>Datum narození</label>
                    <ReactDatez
                        allowPast={true}
                        handleChange={(birthday) => this.editFormData('birthday', moment(birthday))}
                        value={data.birthday && data.birthday.toISOString()}
                        dateFormat={DATE_FORMAT}
                        locale={'cs'}
                    />
                    <label>Jazyky</label>
                    <Multiselect
                        placeholder="Jazyky"
                        options={this.props.langs.map((lang) => ({value: lang.id, label: lang.name}))}
                        values={data.langs}
                        onChange={(values) => this.editFormData('langs', values)}
                    />
                    {editing && (
                      <React.Fragment>
                          <label>Aktivní</label>
                          <Checkbox checked={data.active} id={`editing-active-checkbox`} onChange={(value) => this.editFormData('active', value)}/>
                      </React.Fragment>
                    )}
                    {!editing && (
                        <React.Fragment>
                            <label>Role</label>
                            <Select placeholder="Role" options={ROLE_OPTIONS} onChange={(value) => this.editFormData('role', value)} />
                            <label>Heslo</label>
                            <input type="password" value={data.password} onChange={(e) => this.editFormData('password', e.target.value)}/>
                        </React.Fragment>
                    )}
                </ModalBody>
                <ModalFooter className="modal-footer">
                    <button className="button button-primary" onClick={this.save}>Uložit</button>
                    <button className="button button-secondary" onClick={this.cancel}>Zrušit</button>
                </ModalFooter>
            </Modal>
        );
    }
}

UserForm.propTypes = {
    langs: ImmutablePropTypes.listOf(PropTypes.instanceOf(ProgrammingLanguage)).isRequired,
    setOpenCallback: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({langs: getLanguages(state)});

export default connect(mapStateToProps)(UserForm);