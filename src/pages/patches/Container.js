import React, {Component} from 'react';
import ImmutablePropTypes from "react-immutable-proptypes";
import PropTypes from "prop-types";
import {bindActionCreators} from "redux";
import {openErrorModal} from "../../modules/modals";
import connect from "react-redux/es/connect/connect";
import PatchModel from "../../models/Patch";
import {approvePatch, createPatch, fetchAllPatches, getAllPatches, releasePatch} from "../../modules/patches";
import Patch from "./components/Patch";
import PatchForm from "./components/PatchForm";
import {fetchAllTickets} from "../../modules/tickets";

class PatchesContainer extends Component {
    componentDidMount() {
        this.props.fetchAllTickets();
        this.props.fetchAllPatches();
    }

    create = () => {
        this.state.open({
            title: 'Vytvořit záplatu',
            onSave: (data) => this.props.createPatch(data).catch(() => this.props.openErrorModal("Nepodařilo se vytvořit záplatu"))
        })
    };

    approve = (patch) => {
        this.props.approvePatch(patch.id).catch(() => this.props.openErrorModal("Nepodařilo se schválit záplatu"))
    };

    release = (patch) => {
        this.props.releasePatch(patch.id).catch(() => this.props.openErrorModal("Nepodařilo se vydat záplatu"))
    };


    render() {
        return (
            <div className="patches">
                {this.props.patches.map((patch) => <Patch onRelease={this.release} onApprove={this.approve} patch={patch} />)}
                <PatchForm setOpenCallback={(open) => this.setState({open})}/>
                <i className="action action-primary fas fa-plus" onClick={this.create}/>
            </div>
        );
    }
}

PatchesContainer.propTypes = {
    patches: ImmutablePropTypes.listOf(PropTypes.instanceOf(PatchModel)).isRequired,
    fetchAllPatches: PropTypes.func.isRequired,
    fetchAllTickets: PropTypes.func.isRequired,
    createPatch: PropTypes.func.isRequired,
    approvePatch: PropTypes.func.isRequired,
    releasePatch: PropTypes.func.isRequired,
    openErrorModal: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({patches: getAllPatches(state)});
const mapDispatchToProps = (dispatch) => bindActionCreators({fetchAllPatches, fetchAllTickets, createPatch, approvePatch, releasePatch, openErrorModal}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(PatchesContainer);