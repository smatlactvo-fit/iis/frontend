import React, {Component} from 'react';
import PropTypes from "prop-types";
import Bug from "./Bug";
import PatchModel from '../../../models/Patch';
import {toDateString} from "../../../utils/functions";

class Patch extends Component {
    state = {open: false};

    render() {
        const {patch, onApprove, onRelease} = this.props;
        return (
            <div className="patch">
                <div className="patch-header">
                    <span>Záplata #{patch.id}</span>
                    <span>
                        {patch.state === "wip" && <button className="button button-primary" onClick={() => onApprove(patch)}>Schválit</button>}
                    </span>
                    <span>
                        {patch.state === "approved" && <button className="button button-primary" onClick={() => onRelease(patch)}>Vydat</button>}

                    </span>
                </div>

                <div>
                    {patch.approved_by && (
                        <React.Fragment>
                            <span>Schválil {patch.approved_by.username} ({toDateString(patch.approved_at)})</span>
                        </React.Fragment>
                    )}
                </div>
                <div>
                    {patch.released_by && (
                        <React.Fragment>
                            <span>Vydal {patch.released_by.username} ({toDateString(patch.released_at)})</span>
                        </React.Fragment>
                    )}
                </div>

                {this.state.open ? (
                    <React.Fragment>
                        <div>
                            <i className="action action-primary fas fa-chevron-up" onClick={() => this.setState({open: false})}/>
                        </div>

                        {patch.bugs.groupBy((bug) => bug.ticketId).map((bugs, ticket) => (
                            <div className="patch-ticket">
                                <span>Ticket #{ticket}</span>
                                {bugs.map((bug) => <Bug bug={bug}/>)}
                            </div>
                        )).toList()}

                    </React.Fragment>
                ) : (
                    <React.Fragment>
                        <i className="action action-primary fas fa-chevron-down" onClick={() => this.setState({open: true})}/>
                    </React.Fragment>
                )}
            </div>
        );
    }
}

Patch.propTypes = {
    patch: PropTypes.instanceOf(PatchModel).isRequired,
    onApprove: PropTypes.func.isRequired,
    onRelease: PropTypes.func.isRequired,
};

export default Patch;