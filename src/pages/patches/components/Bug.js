import React from 'react';
import Checkbox from "../../../components/Checkbox";
import PropTypes from "prop-types";

const Bug = ({bug}) => {
    return (
        <div className="patch-ticket-bug">
            <Checkbox checked={bug.resolved} className="checkbox-round" id={`bug-${bug.id}`} disabled={true}/>
            <span className="ticket-bug-description">{bug.description}</span>
            <span>{bug.module.name}</span>
        </div>
    );
};

Bug.propTypes = {
    bug: PropTypes.instanceOf(Bug).isRequired,
};

export default Bug;