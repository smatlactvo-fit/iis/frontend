import ImmutablePropTypes from "react-immutable-proptypes";
import PropTypes from "prop-types";
import connect from "react-redux/es/connect/connect";
import {Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import React from "react";
import {Map, List} from "immutable";
import Multiselect from "../../../components/Multiselect";
import Bug from "../../../models/Bug";
import {getAllTickets} from "../../../modules/tickets";
import Ticket from "../../../models/Ticket";

const DEFAULT_STATE = {open: false, bugs: Map()};

class PatchForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = DEFAULT_STATE;
        this.props.setOpenCallback(this.open);
    }

    open = ({title, onSave, onCancel}) => {
        this.setState({open: true, title, onSave, onCancel});
    };

    save = () => {
        this.state.onSave && this.state.onSave({bugs: this.state.bugs.toList().flatten(1)});
        this.setState(DEFAULT_STATE)
    };

    cancel = () => {
        this.state.onCancel && this.state.onCancel();
        this.setState(DEFAULT_STATE);
    };

    render() {
        const {bugs, open, title} = this.state;
        return (
            <Modal isOpen={open} size="lg">
                {title && <ModalHeader>{title}</ModalHeader>}
                <ModalBody>
                    {this.props.tickets.map((ticket) => (
                        <React.Fragment>
                            <span>Ticket #{ticket.id}</span>
                            <Multiselect
                                placeholder="Bugy"
                                options={ticket.bugs.filter((bug) => !bug.patchId).map((bug) => ({value: bug.id, label: bug.id}))}
                                values={bugs.get(ticket.id)}
                                onChange={(values) => this.setState((state) => ({bugs: state.bugs.set(ticket.id, List(values))}))}
                            />
                        </React.Fragment>
                    ))}
                </ModalBody>
                <ModalFooter className="modal-footer">
                    <button className="button button-primary" onClick={this.save}>Uložit</button>
                    <button className="button button-secondary" onClick={this.cancel}>Zrušit</button>
                </ModalFooter>
            </Modal>
        );
    }
}

PatchForm.propTypes = {
    tickets: ImmutablePropTypes.listOf(PropTypes.instanceOf(Ticket)).isRequired,
    setOpenCallback: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({tickets: getAllTickets(state)});

export default connect(mapStateToProps)(PatchForm);