import React, {Component} from 'react';
import {bindActionCreators} from "redux";
import PropTypes from "prop-types";

import {login} from "../../modules/users";
import {connect} from "react-redux";

class LoginContainer extends Component {
    constructor(props) {
        super(props);

        this.state = {username: '', password: '', error: false};
    }

    isFormValid() {
        return !!(this.state.username && this.state.password);
    }

    login = () => {
      this.props.login(this.state.username, this.state.password)
          .then(() => this.setState({error: false}))
          .catch(() => this.setState({error: true}));
    };

    render() {
        return (
            <div className="l-login">
                <h1>Bug tracker</h1>

                <div className="form">
                    {this.state.error && (
                        <div>
                            <span className="error">Špatné jméno či heslo</span>
                        </div>
                    )}
                    <div>
                        <input type="text" placeholder="Jméno" onChange={(e) => {this.setState({username: e.target.value})}}/>
                    </div>
                    <div>
                        <input type="password" placeholder="Heslo" onChange={(e) => {this.setState({password: e.target.value})}}/>
                    </div>
                    <div>
                        <button className={`button button-primary ${!this.isFormValid() && 'button-disabled'}`} disabled={!this.isFormValid()} onClick={this.login}>
                            Přihlásit
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}

LoginContainer.propTypes = {
    login: PropTypes.func.isRequired,
};

const mapDispatchToProps = (dispatch) => bindActionCreators({login}, dispatch);

export default connect(null, mapDispatchToProps)(LoginContainer);