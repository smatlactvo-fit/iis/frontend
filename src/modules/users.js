// Actions
import {APP_ACTION_PREFIX} from "../utils/constants";
import {List, Map} from 'immutable';
import {doGet, doPatch, doPost, setAuthToken} from "../core/fetch";
import User from "../models/User";
import {destroyToken, storeToken} from "../core/auth";
import {findIndexById} from "../utils/functions";

const ACTION_PREFIX = `${APP_ACTION_PREFIX}/users`;

const FETCH_IDENTITY = `${ACTION_PREFIX}/FETCH_IDENTITY`;
const FETCH_IDENTITY_SUCCESS = `${ACTION_PREFIX}/FETCH_IDENTITY_SUCCESS`;
const FETCH_IDENTITY_FAILED = `${ACTION_PREFIX}/FETCH_IDENTITY_FAILED`;

const FETCH_ALL_SUCCESS = `${ACTION_PREFIX}/FETCH_ALL_SUCCESS`;
const DELETE_SUCCESS = `${ACTION_PREFIX}/DELETE_SUCCESS`;
const UPDATE_SUCCESS = `${ACTION_PREFIX}/UPDATE_SUCCESS`;
const CREATE_SUCCESS = `${ACTION_PREFIX}/CREATE_SUCCESS`;

const LOGOUT = `${ACTION_PREFIX}/LOGOUT`;

// Reducer
const defaultState = Map({
    loggedUser: null,
    isFetching: false,
    all: List(),
});

export default function reducer(state = defaultState, action = {}) {
    switch (action.type) {
        case FETCH_IDENTITY: return state.set('isFetching', true);
        case FETCH_IDENTITY_SUCCESS: return state.merge({loggedUser: action.payload, isFetching: false});
        case FETCH_IDENTITY_FAILED: return state.set('isFetching', false);
        case LOGOUT: return state.set('loggedUser', null);
        case FETCH_ALL_SUCCESS: return state.set('all', action.payload);
        case DELETE_SUCCESS: return state.removeIn(['all', findIndexById(state.get('all'), action.payload)]);
        case UPDATE_SUCCESS: return state.setIn(['all', findIndexById(state.get('all'), action.payload.id)], action.payload);
        case CREATE_SUCCESS: return state.update('all', (all) => all.push(action.payload));
        default: return state;
    }
}

// Action Creators
export function login(username, password) {
    return (dispatch) => doPost('/auth/token/', {username, password})
        .then((response) => {
            const token = response.data.data.token;
            storeToken(token);
            setAuthToken(token);
            dispatch(fetchIdentity())
        });
}

export function logout() {
    return (dispatch) => {
        destroyToken();
        setAuthToken(null);
        dispatch({type: LOGOUT})
    }
}

export function fetchIdentity() {
    return (dispatch) => {
        dispatch({type: FETCH_IDENTITY});
        doGet('/users/me/')
            .then((response) => dispatch({type: FETCH_IDENTITY_SUCCESS, payload: User.fromServer(response.data.data.user)}))
            .catch(() => dispatch({type: FETCH_IDENTITY_FAILED}));
    }
}

export function fetchAllUsers() {
    return (dispatch) => doGet('/users/')
        .then((response) => dispatch({type: FETCH_ALL_SUCCESS, payload: User.fromServerList(response.data.data.users)}))
}

export function updateUser(id, data) {
    return (dispatch) => doPatch(`/users/${id}/`, User.toServerUpdate(data))
        .then((response) => dispatch({type: UPDATE_SUCCESS, payload: User.fromServer(response.data.data.user)}))
}

export function createUser(data) {
    return (dispatch) => doPost(`/users/`, User.toServerCreate(data))
        .then((response) => dispatch({type: CREATE_SUCCESS, payload: User.fromServer(response.data.data.user)}))
}

// Selectors
export const isFetching = (state) => state.users.get('isFetching');
export const getLoggedUser = (state) => state.users.get('loggedUser');
export const getAllUsers = (state) => state.users.get('all');
