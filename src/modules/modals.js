// Actions
import {APP_ACTION_PREFIX, ERROR_MODAL} from "../utils/constants";
import {Map} from 'immutable';

const ACTION_PREFIX = `${APP_ACTION_PREFIX}/modals`;

const OPEN = `${ACTION_PREFIX}/OPEN`;
const CLOSE = `${ACTION_PREFIX}/CLOSE`;

const defaultState = Map({
    [ERROR_MODAL]: Map({open: false, message: null}),
});

// Reducer
export default function reducer(state = defaultState, action = {}) {
    switch (action.type) {
        case OPEN: return state.mergeIn([action.payload.key], {open: true, ...action.payload});
        case CLOSE: return state.mergeIn([action.payload], defaultState.get(action.payload));
        default: return state;
    }
}

// Action Creators
export function openErrorModal(message) {
    return {type: OPEN, payload: {key: ERROR_MODAL, message}};
}

export function closeErrorModal() {
    return {type: CLOSE, payload: ERROR_MODAL}
}

// Selectors
export const getErrorModalMessage = (state) => state.modals.getIn([ERROR_MODAL, 'message']);
export const isErrorModalOpen = (state) => state.modals.getIn([ERROR_MODAL, 'open']);