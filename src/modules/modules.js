// Actions
import {APP_ACTION_PREFIX} from "../utils/constants";
import {List} from 'immutable';
import {doDelete, doGet, doPatch, doPost} from "../core/fetch";
import Module from "../models/Module";
import {findIndexById} from "../utils/functions";

const ACTION_PREFIX = `${APP_ACTION_PREFIX}/modules`;

const FETCH_ALL_SUCCESS = `${ACTION_PREFIX}/FETCH_ALL_SUCCESS`;
const CREATE_SUCCESS = `${ACTION_PREFIX}/CREATE_SUCCESS`;
const UPDATE_SUCCESS = `${ACTION_PREFIX}/UPDATE_SUCCESS`;
const DELETE_MODULE = `${ACTION_PREFIX}/DELETE_MODULE`;

// Reducer
export default function reducer(state = List(), action = {}) {
    switch (action.type) {
        case FETCH_ALL_SUCCESS: return action.payload;
        case CREATE_SUCCESS: return state.push(action.payload);
        case UPDATE_SUCCESS: return state.set(findIndexById(state, action.payload.id), action.payload);
        case DELETE_MODULE: return state.remove(findIndexById(state, action.payload));
        default: return state;
    }
}

// Action Creators
export function fetchAllModules() {
    return (dispatch) => doGet('/modules/')
        .then((response) => dispatch({type: FETCH_ALL_SUCCESS, payload: Module.fromServerList(response.data.data.modules)}))
}

export function createModule(data) {
    return (dispatch) => doPost('/modules/', data)
        .then((response) => dispatch({type: CREATE_SUCCESS, payload: Module.fromServer(response.data.data.module)}))
}

export function updateModule(id, data) {
    return (dispatch) => doPatch(`/modules/${id}/`, data)
        .then((response) => dispatch({type: UPDATE_SUCCESS, payload: Module.fromServer(response.data.data.module)}))
}

export function deleteModule(id) {
    return (dispatch) => doDelete(`/modules/${id}/`)
        .then((response) => dispatch({type: DELETE_MODULE, payload: id}))
}

// Selectors
export const getAllModules = (state) => state.modules;
