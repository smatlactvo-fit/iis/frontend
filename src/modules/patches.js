// Actions
import {APP_ACTION_PREFIX} from "../utils/constants";
import {List} from 'immutable';
import {doGet, doPatch, doPost} from "../core/fetch";
import Patch from "../models/Patch";
import {findIndexById} from "../utils/functions";

const ACTION_PREFIX = `${APP_ACTION_PREFIX}/patches`;

const FETCH_ALL_SUCCESS = `${ACTION_PREFIX}/FETCH_ALL_SUCCESS`;
const CREATE_SUCCESS = `${ACTION_PREFIX}/CREATE_SUCCESS`;
const UPDATE_SUCCESS = `${ACTION_PREFIX}/UPDATE_SUCCESS`;

// Reducer
export default function reducer(state = List(), action = {}) {
    switch (action.type) {
        case FETCH_ALL_SUCCESS: return action.payload;
        case CREATE_SUCCESS: return state.push(action.payload);
        case UPDATE_SUCCESS: return state.set(findIndexById(state, action.payload.id), action.payload);
        default: return state;
    }
}

// Action Creators
export function fetchAllPatches() {
    return (dispatch) => doGet('/patches/')
        .then((response) => dispatch({type: FETCH_ALL_SUCCESS, payload: Patch.fromServerList(response.data.data.patches)}))
}

export function createPatch(data) {
    return (dispatch) => doPost('/patches/', data)
        .then((response) => dispatch({type: CREATE_SUCCESS, payload: Patch.fromServer(response.data.data.patch)}))
}

export function approvePatch(id) {
    return (dispatch) => doPatch(`/patches/${id}/?approve`)
        .then((response) => dispatch({type: UPDATE_SUCCESS, payload: Patch.fromServer(response.data.data.patch)}))
}

export function releasePatch(id) {
    return (dispatch) => doPatch(`/patches/${id}/?release`)
        .then((response) => dispatch({type: UPDATE_SUCCESS, payload: Patch.fromServer(response.data.data.patch)}))
}


// Selectors
export const getAllPatches = (state) => state.patches;
