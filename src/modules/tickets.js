// Actions
import {APP_ACTION_PREFIX} from "../utils/constants";
import {List} from 'immutable';
import {doGet, doPost} from "../core/fetch";
import Ticket from "../models/Ticket";

const ACTION_PREFIX = `${APP_ACTION_PREFIX}/tickets`;

const FETCH_ALL_SUCCESS = `${ACTION_PREFIX}/FETCH_ALL_SUCCESS`;
const CREATE_SUCCESS = `${ACTION_PREFIX}/CREATE_SUCCESS`;

// Reducer
export default function reducer(state = List(), action = {}) {
    switch (action.type) {
        case FETCH_ALL_SUCCESS: return action.payload;
        case CREATE_SUCCESS: return state.push(action.payload);
        default: return state;
    }
}

// Action Creators
export function fetchAllTickets() {
    return (dispatch) => doGet('/tickets/')
        .then((response) => dispatch({type: FETCH_ALL_SUCCESS, payload: Ticket.fromServerList(response.data.data.tickets)}))
}

export function createTicket(data) {
    return (dispatch) => doPost('/tickets/', data)
        .then((response) => dispatch({type: CREATE_SUCCESS, payload: Ticket.fromServer(response.data.data.patch)}))
}

// Selectors
export const getAllTickets = (state) => state.tickets;
