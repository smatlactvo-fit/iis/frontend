import {combineReducers} from "redux";

import users from './users'
import programmingLanguages from "./programmingLanguages";
import modals from "./modals";
import tickets from "./tickets";
import modules from "./modules";
import patches from "./patches";

export default combineReducers({
    users, programmingLanguages, modals, tickets, modules, patches
});