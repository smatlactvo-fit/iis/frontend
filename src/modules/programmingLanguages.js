// Actions
import {APP_ACTION_PREFIX} from "../utils/constants";
import {List} from 'immutable';
import {doDelete, doGet, doPatch, doPost} from "../core/fetch";
import ProgrammingLanguage from "../models/ProgrammingLanguage";
import {findIndexById} from "../utils/functions";

const ACTION_PREFIX = `${APP_ACTION_PREFIX}/programming-languages`;

const FETCH_ALL_SUCCESS = `${ACTION_PREFIX}/FETCH_ALL_SUCCESS`;
const CREATE_SUCCESS = `${ACTION_PREFIX}/CREATE_SUCCESS`;
const UPDATE_SUCCESS = `${ACTION_PREFIX}/UPDATE_SUCCESS`;
const DELETE_SUCCESS = `${ACTION_PREFIX}/DELETE_SUCCESS`;

// Reducer
export default function reducer(state = List(), action = {}) {
    switch (action.type) {
        case FETCH_ALL_SUCCESS: return action.payload;
        case CREATE_SUCCESS: return state.push(action.payload);
        case UPDATE_SUCCESS: return state.set(findIndexById(state, action.payload.id), action.payload);
        case DELETE_SUCCESS: return state.remove(findIndexById(state, action.payload));
        default: return state;
    }
}

// Action Creators
export function fetchAllProgrammingLanguages() {
    return (dispatch) => doGet('/programming-languages/')
        .then((response) => dispatch({type: FETCH_ALL_SUCCESS, payload: ProgrammingLanguage.fromServerList(response.data.data.langs)}))
}

export function createProgrammingLanguage(name) {
    return (dispatch) => doPost('/programming-languages/', {name})
        .then((response) => dispatch({type: CREATE_SUCCESS, payload: ProgrammingLanguage.fromServer(response.data.data.lang)}))
}

export function deleteProgrammingLanguage(id) {
    return (dispatch) => doDelete(`/programming-languages/${id}/`)
        .then(() => dispatch({type: DELETE_SUCCESS, payload: id}))
}

export function updateProgrammingLanguage(id, name) {
    return (dispatch) => doPatch(`/programming-languages/${id}/`, {name})
        .then((response) => dispatch({type: UPDATE_SUCCESS, payload: ProgrammingLanguage.fromServer(response.data.data.lang)}))
}

// Selectors
export const getLanguages = (state) => state.programmingLanguages;