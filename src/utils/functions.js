import {List, Record} from "immutable";
import {DATE_FORMAT} from "./constants";

export function createModel(name, fromJson) {
    const Class = Record(fromJson({}), name);
    Class.fromServer = (json = {}) => new Class(fromJson(json));
    Class.fromServerList = (json = []) => List(json.map(Class.fromServer));

    return Class;
}

export function toDateString(moment) {
    return moment && moment.format(DATE_FORMAT);
}

export function findIndexById(list, id) {
    return list.findIndex((item) => item.id === id);
}

export function identity(x) {
    return x;
}