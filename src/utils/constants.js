export const APP_ACTION_PREFIX = 'IIS';
export const ERROR_MODAL = 'error_modal';
export const DATE_FORMAT = 'DD.MM.YYYY';

export const ROLES = {
    ROLE_ADMIN: 'admin',
    ROLE_PROGRAMMER: 'programmer',
    ROLE_USER: 'user',
};

export const ROLE_LABELS = {
    [ROLES.ROLE_ADMIN]: 'Administrátor',
    [ROLES.ROLE_PROGRAMMER]: 'Programátor',
    [ROLES.ROLE_USER]: 'Uživatel',
};