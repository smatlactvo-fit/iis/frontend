import React from 'react';
import {render} from 'react-dom';
import App from './layout/App';
import {Provider} from 'react-redux';
import {applyMiddleware, compose, createStore} from "redux";
import thunk from 'redux-thunk';
import reducer from './modules';
import {identity} from "./utils/functions";
import {getToken} from "./core/auth";
import {fetchIdentity} from "./modules/users";
import {setAuthToken, setBaseUrl} from "./core/fetch";
import 'react-datez/dist/css/react-datez.css';

const enhancer = compose(
    applyMiddleware(thunk),
    window.__REDUX_DEVTOOLS_EXTENSION__ ? window.__REDUX_DEVTOOLS_EXTENSION__() : identity,
);

const store = createStore(reducer, enhancer);

setBaseUrl(process.env.REACT_APP_API_URL);
const token = getToken();
if(token) {
    setAuthToken(token);
    store.dispatch(fetchIdentity())
}

render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);
