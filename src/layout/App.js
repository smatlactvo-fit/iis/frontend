import React from 'react';
import PropTypes from 'prop-types';
import '../assets/sass/main.scss';
import {BrowserRouter as Router, Redirect, Route, Switch} from "react-router-dom";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import Loader from 'react-loader-spinner'
import MainLayout from "./MainLayout";
import {getLoggedUser, isFetching, logout} from "../modules/users";
import User from "../models/User";

import LoginContainer from "../pages/login/Container";
import AdministrationContainer from "../pages/administration/Container";
import BugsContainer from "../pages/bugs/Container";
import PatchesContainer from "../pages/patches/Container";

const App = ({loggedUser, isFetching, logout}) => (
    <Router basename={process.env.REACT_APP_BASE_PATH}>
        <React.Fragment>
            <Route path="/prihlaseni" render={() => isFetching ? <Spinner/> : (!loggedUser ? <LoginContainer/> : <Redirect to={{pathname: '/'}}/>)}/>
            <Switch>
                <Protected isLogged={!!loggedUser} isFetching={isFetching}>
                    <MainLayout user={loggedUser} logout={logout}>
                        <Route exact path="/" render={() => <Redirect to="/bugy"/>}/>
                        <Route path="/nastaveni/:tab?" component={AdministrationContainer}/>
                        <Route path="/bugy" component={BugsContainer}/>
                        <Route path="/zaplaty" component={PatchesContainer}/>
                    </MainLayout>
                </Protected>
            </Switch>
        </React.Fragment>
    </Router>
);

const Protected = ({ isLogged, isFetching, ...props }) => (
    isFetching
        ? <Spinner/>
        : (isLogged
            ? <Route {...props}/>
            : <Redirect to="/prihlaseni"/>)
);

const Spinner = () => (
    <div className="l-login">
        <div className="spinner">
            <Loader type="Oval" color="#7471D0" height={80} width={80} />
        </div>
    </div>
);

App.propTypes = {
    isFetching: PropTypes.bool.isRequired,
    loggedUser: PropTypes.instanceOf(User),
    logout: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
    loggedUser: getLoggedUser(state),
    isFetching: isFetching(state),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({logout}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(App);
