import React from 'react';
import LeftSidebar from "./LeftSidebar";
import ErrorModal from "../modals/Error";

const MainLayout = ({user, children, logout}) => (
    <div className="l-main">
        <LeftSidebar user={user} logout={logout}/>
        <div className="content">
            {children}
        </div>
        <ErrorModal />
    </div>
);

export default MainLayout;