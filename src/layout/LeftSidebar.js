import React from 'react';
import PropTypes from 'prop-types';
import {NavLink} from "react-router-dom";
import User from "../models/User";
import {ROLES} from "../utils/constants";

const LeftSidebar = ({user, logout}) => {
    return (
        <div className="sidebar">
            <div className="sidebar-items-item" >
                <i className="icon icon-primary fas fa-user-circle"/> <span style={{color: 'black'}}>{user.username}</span>
            </div>
            <div className="sidebar-items-item" onClick={logout}>
                <i className="icon fas fa-sign-out-alt"/>
            </div>
            <nav className="sidebar-items">
                <NavLink className="sidebar-items-item" to="/bugy" activeClassName="sidebar-items-active">
                    <i className="icon fas fa-bug"/> BUGY
                </NavLink>
                <NavLink className="sidebar-items-item" to="/zaplaty" activeClassName="sidebar-items-active">
                    <i className="icon fas fa-compress"/> ZÁPLATY
                </NavLink>
                {user.role === ROLES.ROLE_ADMIN && (
                    <NavLink className="sidebar-items-item" to="/nastaveni" activeClassName="sidebar-items-active">
                        <i className="icon fas fa-cog"/> Nastavení
                    </NavLink>
                )}
            </nav>
        </div>
    )
};

LeftSidebar.propTypes = {
    user: PropTypes.instanceOf(User),
    logout: PropTypes.func.isRequired,
};

export default LeftSidebar;

