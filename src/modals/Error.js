import React from 'react';
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {closeErrorModal, getErrorModalMessage, isErrorModalOpen} from "../modules/modals";
import {Modal, ModalBody, ModalHeader} from "reactstrap";

const ErrorModal = ({isOpen, message, closeErrorModal}) => (
    <Modal isOpen={isOpen} toggle={closeErrorModal} className="modal-error">
        <ModalHeader toggle={closeErrorModal}>Chyba</ModalHeader>
        <ModalBody>{message}</ModalBody>
    </Modal>
);

ErrorModal.propTypes = {
    isOpen: PropTypes.bool.isRequired,
    message: PropTypes.string,
    closeErrorModal: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({isOpen: isErrorModalOpen(state), message: getErrorModalMessage(state)});
const mapDispatchToProps = (dispatch) => bindActionCreators({closeErrorModal}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ErrorModal);