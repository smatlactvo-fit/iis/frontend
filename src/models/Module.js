import {createModel} from "../utils/functions";
import ProgrammingLanguage from "./ProgrammingLanguage";
import User from "./User";

const Module = createModel('Module', (json) => ({
    id: json.id,
    name: json.name,
    lang: ProgrammingLanguage.fromServer(json.lang),
    admin: User.fromServer(json.admin),
}));

export default Module;