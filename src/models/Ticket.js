import {createModel} from "../utils/functions";
import Bug from "./Bug";
import User from "./User";
import moment from "moment";

const Ticket = createModel('Ticket', (json) => ({
    id: json.id,
    watched: json.watched,
    created_by: User.fromServer(json.created_by),
    created_at: moment(json.created_at),
    bugs: Bug.fromServerList(json.bugs),
}));

export default Ticket;