import {createModel} from "../utils/functions";
import moment from "moment";
import ProgrammingLanguage from "./ProgrammingLanguage";

const User = createModel('User', (json) => ({
    id: json.id,
    username: json.username,
    birthday: json.birthday && moment(json.birthday),
    role: json.role,
    langs: ProgrammingLanguage.fromServerList(json.langs),
    active: json.active,
}));

User.toServerCreate = (data) => ({
    username: data.username,
    birthday: data.birthday,
    langs: data.langs,
    role: data.role,
    password: data.password
});

User.toServerUpdate = (data) => ({
    username: data.username,
    birthday: data.birthday,
    langs: data.langs,
    active: data.active
});

export default User;