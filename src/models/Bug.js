import {createModel} from "../utils/functions";
import Module from './Module'

const Bug = createModel('Bug', (json) => ({
    id: json.id,
    description: json.description,
    priority: json.priority,
    resolved: json.resolved,
    module: Module.fromServer(json.module),
    patchId: json.patch,
    ticketId: json.ticket,
}));

export default Bug;