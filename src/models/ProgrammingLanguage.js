import {createModel} from "../utils/functions";

const ProgrammingLanguage = createModel('ProgrammingLanguage', (json) => ({
    id: json.id,
    name: json.name
}));

export default ProgrammingLanguage;