import {createModel} from "../utils/functions";
import User from "./User";
import Bug from "./Bug";
import moment from "moment";

const Patch = createModel('Patch', (json) => ({
    id: json.id,
    state: json.state,
    approved_by: json.approved_by && User.fromServer(json.approved_by),
    approved_at: moment(json.approved_at),
    released_by: json.released_by && User.fromServer(json.released_by),
    released_at: moment(json.released_at),
    bugs: Bug.fromServerList(json.bugs),
}));

export default Patch;