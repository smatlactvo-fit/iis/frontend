import axios from 'axios';

axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.headers.patch['Content-Type'] = 'application/json';

export function setBaseUrl(baseUrl) {
    axios.defaults.baseURL = baseUrl;
}

export function setAuthToken(token) {
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
}

export function doGet(url, queryParams) {
    return axios.get(url, queryParams);
}

export function doPatch(url, body) {
    return axios.patch(url, body);
}

export function doPost (url, body) {
    return axios.post(url, body);
}

export function doDelete (url, queryParams) {
    return axios.delete(url, queryParams)
}